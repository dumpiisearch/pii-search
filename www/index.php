<!doctype html>
<html lang="en">
<head>
	<?php readfile("../include/head.html"); ?>
    <title>PII Search</title>
</head>
<body>

<header>
    <div class="container">
        <h1>Personally-Identifiable Information Search</h1>

        <noscript>
            <div class="alert danger">
                <p>
                    You need to
                    <a href="https://www.enable-javascript.com/">enable JavaScript</a>
                    before you'll be able to use this tool.
                </p>
                <p>Why? Read the footer text.</p>
            </div>
        </noscript>
    </div>
</header>

<main>
    <div class="container">
        <p>Any given field is optional, but you should fill in at least one.</p>

        <form action="search.php" method="POST" onsubmit="return false">
            <div class="row">
                <div class="four columns">
                    <label for="email">Email address</label>
                </div>
                <div class="eight columns">
                    <input type="email"
                           id="email"
                           class="u-full-width"
                           placeholder="user@example.com"
                           data-class="EMAIL">
                </div>
            </div>

            <div class="row">
                <div class="four columns">
                    <label for="phone">Phone number</label>
                </div>
                <div class="eight columns">
                    <input type="tel"
                           id="phone"
                           class="u-full-width"
                           placeholder="555-867-5309 or 5558675309"
                           pattern="^(\d{10}|\d{3}-\d{3}-\d{4})$"
                           data-class="PHONE">
                </div>
            </div>

            <div class="row">
                <div class="four columns">
                    <label for="ipv4">IPv4 address</label>
                </div>
                <div class="eight columns">
					<?php
					$ip = array_key_exists("HTTP_X_FORWARDED_FOR", $_SERVER)
						? $_SERVER["HTTP_X_FORWARDED_FOR"]
						: $_SERVER["REMOTE_ADDR"];
					$ip4 = strpos($ip, ":") === false ? $ip : "";
					?>
                    <input type="text"
                           id="ipv4"
                           class="u-full-width"
                           placeholder="255.255.255.255"
                           value="<?= $ip4 ?>"
                           data-class="IPv4">
                </div>
            </div>

			<?php if (isset($_GET["ssn"])): ?>
                <div class="row">
                    <div class="four columns">
                        <label for="ssn">Social Security Number</label>
                    </div>
                    <div class="eight columns">
                        <input type="text"
                               id="ssn"
                               class="u-full-width"
                               placeholder="000-00-0000 or 000000000"
                               pattern="^(\d{9}|\d{3}-\d{2}-\d{4})$"
                               data-class="SSN">
                    </div>
                </div>
			<?php endif; ?>

            <div class="row">
                <div class="four columns">
                    <label for="handle">Twitter Handle</label>
                </div>
                <div class="eight columns">
                    <input type="text"
                           id="handle"
                           class="u-full-width"
                           placeholder="@twitter"
                           pattern="^@(\w+)$"
                           data-class="HANDLE">
                </div>
            </div>

            <button type="button" class="button-primary" onclick="hashForm(this.parentElement)">Search</button>
            <button type="reset" class="button">Clear</button>
        </form>
    </div>
</main>

<footer>
    <div class="container">
        <p>
            The information you submit will be immediately and irreversibly
            <a href="https://en.wikipedia.org/wiki/Cryptographic_hash_function" target="_blank">hashed</a> using
            <a href="https://blake2.net/" target="_blank">BLAKE2s</a> in your browser before it is uploaded the server.
            The server then checks the hashes against a database of already-hashed information
            that was found while searching through the dump files.
        </p>
        <p><b>TL;DR: None of the information you enter is ever uploaded to the server.</b></p>
        <p>
            Don't believe me? Check out the code for
            <a href="https://gitlab.com/dumpiisearch/pii-search">this search website</a> or
            <a href="https://gitlab.com/dumpiisearch/pii-indexer">the indexer that generated the database</a>.
        </p>
		<?php readfile("../include/footer.html"); ?>
    </div>
</footer>

<script src="js/blake2s.min.js"></script>
<script src="js/hash.js"></script>
</body>
</html>
