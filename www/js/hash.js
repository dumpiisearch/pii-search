const normalizers = {
    "handle": new RegExp(/@/g),
    "phone": new RegExp(/[()\-. ]/g),
    "ssn": new RegExp(/[- ]/g),
}

/**
 * Normalize a string to the standard format
 * that the indexer used when hashing.
 * @param {string} str - value to be normalized
 * @param {string} type - the type of value to normalize
 * @returns {string} - normalized value
 */
function normalize(str, type) {
    str = str.toLowerCase().trim();

    if (normalizers.hasOwnProperty(type)) {
        str = str.replace(normalizers[type], "");
    }
    return str;
}

/**
 * Hash all fields in the form before submitting.
 * @param {HTMLFormElement} form - form being submitted
 */
function hashForm(form) {
    // Remove last submit attempt
    let oldInput;
    while ((oldInput = form.querySelector('input[type="hidden"]')) != null) {
        oldInput.remove();
    }

    for (let input of form.querySelectorAll("input")) {
        let value = input.value.toString();

        // Skip empty fields
        if (value.length === 0) {
            continue;
        }

        value = normalize(value, input.id);
        value = new BLAKE2s(32).update(new TextEncoder().encode(value)).hexDigest();

        // Add to form
        let hiddenInput = document.createElement("input");
        hiddenInput.type = "hidden";
        hiddenInput.name = input.dataset["class"];
        hiddenInput.value = value;
        form.appendChild(hiddenInput);
    }

    form.submit();
}
