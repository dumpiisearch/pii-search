<!doctype html>
<html lang="en">
<head>
	<?php readfile("../include/head.html"); ?>
    <title>PII Search Results</title>
</head>
<body>
<header>
    <div class="container">
        <h1>Personally-Identifiable Information Search</h1>
        <h2>Results</h2>
    </div>
</header>

<main>
    <div class="container">
        <p>
            You searched by: <?= implode(", ", array_keys($_POST)) ?>
        </p>

		<?php
		require_once "../include/do_search.php";

		try {
			$results = search();

			if (count($results) === 0) {
				?>
                <div class="alert success">
                    <p>Good news: that information was not found!</p>
                </div>
				<?php
			} else {
				?>
                <div class="alert danger">
                    <p>Bad news: that information was found!</p>
                </div>
                <table id="results">
                    <thead>
                    <tr>
                        <th scope="col">Matched</th>
                        <th scope="col">File</th>
                        <th scope="col"
                            title="The character index in the file at which this information was found (0-indexed)">
                            Location
                        </th>
                    </tr>
                    </thead>
                    <tbody>
					<?php
					foreach ($results as $result) {
						?>
                        <tr class="pii-<?= $result->class ?>">
                            <td>
                                <span class="iconify" data-icon="<?= $result->getIcon() ?>"></span>
								<?= $result->class ?>
                            </td>
                            <td>
                                <span class="iconify" data-icon="mdi-download"></span>
                                <a href="https://blueleaks.io/<?= $result->file ?>" download>
									<?= $result->file ?>
                                </a>
                            </td>
                            <td><?= $result->pos ?></td>
                        </tr>
						<?php
					}
					?>
                    </tbody>
                </table>
				<?php
			}
		} catch (Exception $exception) {
			?>
            <div class="alert danger">
                <h3>Error</h3>
                <pre><?= $exception->getMessage() ?></pre>
                <p>
                    <button class="button-primary" onclick="window.location.reload()">Retry</button>
                </p>
            </div>
			<?php
		}
		?>
    </div>
</main>

<footer>
    <div class="container">
        <p>
            If you want to try again with different information:
            <button class="button-primary" onclick="window.history.back()">Go back</button>
        </p>
        <p>There's a chance something didn't show up here.
            You can always <a href="https://blueleaks.io/">browse the dump</a> yourself
            or <a href="https://gitlab.com/dumpiisearch">help make this tool better</a>.
        </p>
	    <?php readfile("../include/footer.html"); ?>
    </div>
</footer>

<script src="https://code.iconify.design/1/1.0.6/iconify.min.js"></script>
</body>
</html>
