<?php

require_once "PII.php";

/**
 * @return PII[]
 * @throws Exception
 */
function search() : array
{
	if (count($_POST) === 0) {
		return [];
	}

	if (($ini = parse_ini_file("../db.ini")) === false) {
		throw new Exception();
	}

	$pdo = new PDO($ini["dsn"], $ini["username"], $ini["password"]);
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$matches = [];

	$sql = <<<SQL
SELECT class, info, file, pos FROM PII WHERE class = :c AND info = :h
ORDER BY file, class, pos
SQL;

	foreach ($_POST as $class => $hash) {
		$stmt = $pdo->prepare($sql);

		$stmt->execute(["c" => $class, "h" => $hash]);
		$matches = array_merge($matches, $stmt->fetchAll(PDO::FETCH_CLASS, "PII"));
	}

	return $matches;
}
