<?php

class PII
{
	/**
	 * @var string The type of PII
	 */
	public string $class;

	/**
	 * @var string The value as a hash
	 */
	public string $info;

	/**
	 * @var string The filename it was found it
	 */
	public string $file;

	/**
	 * @var int The character index into the file at which it was found
	 */
	public int $pos;

	public function getIcon() : string
	{
		switch ($this->class) {
			case "EMAIL":
				return "mdi-email";
			case "PHONE":
				return "mdi-phone";
			case "SSN":
				return "mdi-ticket";
			case "HANDLE":
				return "mdi-twitter";
			default:
				return "";
		}
	}
}
